package com.balancesheet.server;

public class TransactionData
{
	public String description;
	public float income;
	public float expense;
	public long timeStamp;
	public String transactionId;

	public TransactionData()
	{
	}
}
