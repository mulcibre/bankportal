package com.balancesheet.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import balancesheet.SimpleDBUtils;

import com.amazonaws.services.simpledb.model.Attribute;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.google.gson.Gson;

/**
 * Servlet implementation class login
 */
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//create a boolean that we'll set TRUE when user is authenticated
		Boolean validated = false;
		
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e) { /* report an error */
		}

		String loginDataString = jb.toString();

		Gson gson = new Gson();
		LoginData loginData = gson.fromJson(loginDataString, LoginData.class);

			List<Attribute> attributes = SimpleDBUtils.FetchItem("domainPasswords", loginData.username);
			if(attributes != null)
			{
			for (Attribute attribute : attributes)
			{
				if (attribute.getName().equals("PASSWORD"))
				{
					if(attribute.getValue().equals(loginData.password))
					{
						validated = true;
						
						//TODO (sgluss): huge security hole below, username should be encrypted
						Cookie loginCookie = new Cookie("login", loginData.username);
						loginCookie.setPath("/");
						response.addCookie(loginCookie);
					}
				}
			}
			}
			//send a response back to the client
			response.setContentType("text/html");
		    PrintWriter out = response.getWriter();
		    out.println(gson.toJson(validated));
	}
	
}
