package com.balancesheet.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import balancesheet.LoginUtils;
import balancesheet.SimpleDBUtils;

import com.amazonaws.services.simpledb.model.Attribute;
import com.amazonaws.services.simpledb.model.Item;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.google.gson.Gson;

/**
 * Servlet implementation class transaction
 */
public class transaction extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public transaction()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// check to see if user is logged in
		String userID = LoginUtils.CheckForLogin(request);
		if (userID.equals(""))
		{
			LoginUtils.OutputLoginError(response);
			return;
		}

		String lookupKey = userID + "_TRANSACTION";

		List<Item> items = SimpleDBUtils.FetchAllItemsWithSubstring("allUserTransactions", lookupKey);

		List<TransactionData> transactionList = new ArrayList<TransactionData>();

		for (Item item : items)
		{
			TransactionData transactionData = new TransactionData();

			List<Attribute> attributes = item.getAttributes();

			transactionData.transactionId = item.getName();

			for (Attribute attribute : attributes)
			{
				if (attribute.getName().equals("DESCRIPTION"))
				{
					transactionData.description = attribute.getValue();
				}
				else if (attribute.getName().equals("INCOME"))
				{
					transactionData.income = Float.parseFloat(attribute.getValue());
				}
				else if (attribute.getName().equals("EXPENSE"))
				{
					transactionData.expense = Float.parseFloat(attribute.getValue());
				}
				else if (attribute.getName().equals("TIMESTAMP"))
				{
					transactionData.timeStamp = Long.parseLong(attribute.getValue());
				}
			}

			transactionList.add(transactionData);
		}

		Gson gson = new Gson();
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println(gson.toJson(transactionList));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		// check to see if user is logged in
		String userID = LoginUtils.CheckForLogin(request);
		if (userID.equals(""))
		{
			LoginUtils.OutputLoginError(response);
			return;
		}

		TransactionData[] transactionDataArray = NetworkParsingUtils.GetObjectFromRequest(request, TransactionData[].class);

		for (TransactionData transactionData : transactionDataArray)
		{

			String transactionId = transactionData.transactionId;

			if (transactionId.equals("new"))
			{
				transactionId = userID + "_TRANSACTION_" + UUID.randomUUID().toString();
				transactionData.transactionId = transactionId;
			}

			ReplaceableAttribute descriptionAttr = new ReplaceableAttribute("DESCRIPTION", transactionData.description, Boolean.TRUE);
			ReplaceableAttribute incomeAttr = new ReplaceableAttribute("INCOME", Float.toString(transactionData.income), Boolean.TRUE);
			ReplaceableAttribute expenseAttr = new ReplaceableAttribute("EXPENSE", Float.toString(transactionData.expense), Boolean.TRUE);
			ReplaceableAttribute timeStampAttr = new ReplaceableAttribute("TIMESTAMP", Long.toString(transactionData.timeStamp), Boolean.TRUE);

			List<ReplaceableAttribute> attributes = new ArrayList<ReplaceableAttribute>(2);
			attributes.add(descriptionAttr);
			attributes.add(incomeAttr);
			attributes.add(expenseAttr);
			attributes.add(timeStampAttr);

			SimpleDBUtils.SaveItem("allUserTransactions", transactionId, attributes);
		}

		NetworkParsingUtils.SendResponse(response, transactionDataArray);
	}

	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		// check to see if user is logged in
		String userID = LoginUtils.CheckForLogin(request);
		if (userID.equals(""))
		{
			LoginUtils.OutputLoginError(response);
			return;
		}

		String transactionId = request.getParameter("transactionId");
		if (transactionId.length() > 0)
		{
			SimpleDBUtils.DeleteItem("allUserTransactions", transactionId);
		}

		NetworkParsingUtils.SendResponse(response, transactionId);
	}

}
