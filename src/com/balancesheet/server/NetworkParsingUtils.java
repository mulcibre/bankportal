package com.balancesheet.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class NetworkParsingUtils
{

	@SuppressWarnings("unchecked")
	public static <T> T GetObjectFromRequest(HttpServletRequest request,
			@SuppressWarnings("rawtypes") Class classOfT)
	{
		StringBuffer jb = new StringBuffer();
		String line = null;
		try
		{
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e)
		{ /* report an error */
		}

		String dataString = jb.toString();

		Gson gson = new Gson();
		return (T) gson.fromJson(dataString, classOfT);
	}

	public static <T> void SendResponse(HttpServletResponse response,
			T responseObject) throws IOException
	{
		Gson gson = new Gson();
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println(gson.toJson(responseObject));
	}
}
