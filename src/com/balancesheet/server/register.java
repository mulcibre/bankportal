package com.balancesheet.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import balancesheet.SimpleDBUtils;

import com.amazonaws.services.simpledb.model.Attribute;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;

/**
 * Servlet implementation class register
 */
public class register extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public register()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		RegisterData registerData = NetworkParsingUtils.GetObjectFromRequest(
				request, RegisterData.class);

		List<Attribute> attributes = SimpleDBUtils.FetchItem("domainPasswords",
				registerData.userName);
		Boolean registrationExists = attributes != null
				&& attributes.size() > 0;

		if (registrationExists)
		{
			// reg already exists, email you your password?
			NetworkParsingUtils.SendResponse(response, false);
			return;
		}

		else
		{
			ReplaceableAttribute userNameAttr = new ReplaceableAttribute(
					"USERNAME", registerData.userName, Boolean.TRUE);
			ReplaceableAttribute passwordAttr = new ReplaceableAttribute(
					"PASSWORD", registerData.password, Boolean.TRUE);
			ReplaceableAttribute emailAttr = new ReplaceableAttribute("EMAIL",
					registerData.email, Boolean.TRUE);

			// create a new list of attributes that we'll use to populate a new
			// user account
			List<ReplaceableAttribute> newAttributes = new ArrayList<ReplaceableAttribute>();

			newAttributes.add(userNameAttr);
			newAttributes.add(passwordAttr);
			newAttributes.add(emailAttr);

			SimpleDBUtils.SaveItem("domainPasswords", registerData.userName,
					newAttributes);

			// registration entry has been created in the database
			NetworkParsingUtils.SendResponse(response, true);

			// create a cookie to login with the new account
			// TODO (sgluss): huge security hole below, username should be
			// encrypted
			Cookie loginCookie = new Cookie("login", registerData.userName);
			loginCookie.setPath("/");
			response.addCookie(loginCookie);
			return;
		}
	}

}
