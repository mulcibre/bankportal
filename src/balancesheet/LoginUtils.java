package balancesheet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginUtils
{

	public static String CheckForLogin(HttpServletRequest request)
	{
		Cookie[] cookies = request.getCookies();
		if (cookies != null)
		{
			for (Cookie cookie : cookies)
			{
				String cookieName = "";

				if (cookie != null)
				{
					cookieName = cookie.getName();
				}

				if (cookieName.equals("login"))
				{
					return cookie.getValue();
				}
			}
		}
		return "";
	}

	public static void OutputLoginError(HttpServletResponse response)
	{
		response.setContentType("text/html");

		PrintWriter out = null;
		try
		{
			out = response.getWriter();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.println(" user is not logged in");
	}
}
