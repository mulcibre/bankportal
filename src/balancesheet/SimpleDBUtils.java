package balancesheet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpledb.AmazonSimpleDB;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.model.Attribute;
import com.amazonaws.services.simpledb.model.CreateDomainRequest;
import com.amazonaws.services.simpledb.model.DeleteAttributesRequest;
import com.amazonaws.services.simpledb.model.GetAttributesRequest;
import com.amazonaws.services.simpledb.model.GetAttributesResult;
import com.amazonaws.services.simpledb.model.Item;
import com.amazonaws.services.simpledb.model.PutAttributesRequest;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.amazonaws.services.simpledb.model.SelectRequest;
import com.amazonaws.services.simpledb.model.SelectResult;

public class SimpleDBUtils
{
	private static BasicAWSCredentials _basicAWSCredentials = new BasicAWSCredentials("AKIAIBA6KSISDD4LX6MA", "iRECPqWwzCNRessiusbtnC9aPH1/Yc3L9OUKa8xY");
	private static AmazonSimpleDB _sdb = new AmazonSimpleDBClient(_basicAWSCredentials);
	private static Region _usWest2 = Region.getRegion(Regions.US_WEST_2);
	private static java.util.HashMap<String, Boolean> _domainMap = new HashMap<String, Boolean>();

	public static void SetRegion()
	{
		_sdb.setRegion(_usWest2);
	}

	private static Boolean CreateDomainIfNotExist(String domainName)
	{
		try
		{
			SetRegion();
			// Create a domain
			if (!_domainMap.containsKey(domainName))
			{
				System.out.println("Creating domain called " + domainName + ".\n");
				_sdb.createDomain(new CreateDomainRequest(domainName));
				_domainMap.put(domainName, true);
			}
		}
		catch (AmazonServiceException ase)
		{
			return false;
		}
		catch (AmazonClientException ace)
		{
			return false;
		}

		return true;
	}

	public static Boolean SaveItem(String domainName, String itemName, List<ReplaceableAttribute> attributes)
	{
		// Check input values

		try
		{
			CreateDomainIfNotExist(domainName);
			PutAttributesRequest putAttributesRequest = new PutAttributesRequest(domainName, itemName, attributes);
			_sdb.putAttributes(putAttributesRequest);
		}
		catch (Exception exception)
		{
			return false;
		}
		return true;
	}

	public static Boolean DeleteItem(String domainName, String itemName)
	{
		try
		{
			DeleteAttributesRequest deleteAttributesRequest = new DeleteAttributesRequest();
			deleteAttributesRequest.setDomainName(domainName);
			deleteAttributesRequest.setItemName(itemName);
			_sdb.deleteAttributes(deleteAttributesRequest);
		}
		catch (Throwable ex)
		{
			System.out.println(ex.toString());
			return false;
		}
		return true;
	}

	public static List<Attribute> FetchItem(String domainName, String itemName)
	{
		// Check input values

		try
		{
			CreateDomainIfNotExist(domainName);
			GetAttributesRequest getAttributesRequest = new GetAttributesRequest(domainName, itemName);
			GetAttributesResult getAttributesResult = _sdb.getAttributes(getAttributesRequest);
			return getAttributesResult.getAttributes();
		}
		catch (Exception exception)
		{
			return null;
		}
	}

	public static List<Item> FetchAllItemsWithSubstring(String domainName, String subString)
	{
		// Check input values

		try
		{
			CreateDomainIfNotExist(domainName);

			// The fastest way to query for items is to sort on the itemname
			// using the LIKE keyword
			String query = "select * from " + domainName + " where itemName() like '" + subString + "%' limit 1000";

			return getItems(query);
		}
		catch (Exception exception)
		{
			return null;
		}
	}

	protected static List<Item> getItems(String query)
	{
		List<Item> items = new ArrayList<Item>();

		String nextToken = null;

		do
		{
			SelectRequest selectRequest = new SelectRequest(query);
			selectRequest.setConsistentRead(true);

			if (nextToken != null)
			{
				selectRequest.setNextToken(nextToken);
			}

			SelectResult result = _sdb.select(selectRequest);
			items.addAll(result.getItems());
			nextToken = result.getNextToken();

		}
		while (nextToken != null);

		return items;
	}
}