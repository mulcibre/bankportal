(function(){
	var rt = window.registrationTools = window.registrationTools || {};
	
	rt.register = function registration()
	{
		var username = $("#Username").val();
		var password = $("#Password").val();
		var passwordConfirm = $("#PasswordConfirm").val();
		var email = $("#Email").val();
		var emailConfirm = $("#EmailConfirm").val();
		
		if(!username || username == "" || 
			!password ||  password == "" ||
			!passwordConfirm ||  passwordConfirm == "" ||
			!email  	 || email == "" ||
			!emailConfirm  	 || emailConfirm == "")
		{
			alert("Please fill in all fields");
			return false;
		}
		
		//check for underscores in username to prevent exploit
		if(username.indexOf('_') != -1)
			{
			alert("Use of _ character in username not allowed");
			return false;
			}
		
		if(password != passwordConfirm || 
		   email != emailConfirm)
	    {
			alert("Password and email must match Confirm fields");
			return false;
	    }
		
		if(!validateEmail(email))
		{
			alert("Please enter a valid Email");
			return false;
		}
		
		var registrationData = {userName:username, password:password, email:email};
		$.ajax({
	        type: 'POST',
	        url: '../register',
	        data: JSON.stringify(registrationData),
	        
	        success: function (data) 
	        {
	        	if(data)
	        		{
	        		alert("Registration entry added successfully");
	        		window.location = qualifyURL("../BalancePage");
	        		}
	        	else
	        		{
	        		alert("Registration entry already exists!");
	        		}
	        	},
	        	
	        contentType: "application/json",
	        dataType: 'json'
	    });
	};
	
	function escapeHTML(s) {
	    return s.split('&').join('&amp;').split('<').join('&lt;').split('"').join('&quot;');
	}
	
	function qualifyURL(url) {
	    var el= document.createElement('div');
	    el.innerHTML= '<a href="'+escapeHTML(url)+'">x</a>';
	    return el.firstChild.href;
	}
	
	function validateEmail(email) {
	    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}
}());