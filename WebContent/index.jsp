<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="com.amazonaws.*"%>
<%@ page import="com.amazonaws.auth.*"%>
<%@ page import="com.amazonaws.services.ec2.*"%>
<%@ page import="com.amazonaws.services.ec2.model.*"%>
<%@ page import="com.amazonaws.services.s3.*"%>
<%@ page import="com.amazonaws.services.s3.model.*"%>
<%@ page import="com.amazonaws.services.dynamodbv2.*"%>
<%@ page import="com.amazonaws.services.dynamodbv2.model.*"%>

<%!// Share the client objects across threads to
	// avoid creating new clients for each web request
	private AmazonEC2 ec2;
	private AmazonS3 s3;
	private AmazonDynamoDB dynamo;%>

<%
	/*
     * AWS Elastic Beanstalk checks your application's health by periodically
     * sending an HTTP HEAD request to a resource in your application. By
     * default, this is the root or default resource in your application,
     * but can be configured for each environment.
     *
     * Here, we report success as long as the app server is up, but skip
     * generating the whole page since this is a HEAD request only. You
     * can employ more sophisticated health checks in your application.
     */
    if (request.getMethod().equals("HEAD")) return;
%>

<%
	Cookie[] cookies = request.getCookies();
    		 Cookie loginCookie = null;
    		if(cookies != null)
    		{
    		 for(Cookie cookie : cookies)
    		 {
    			 String cookieName = "";
    			 
    			 if (cookie != null)
    			 {
    				 cookieName = cookie.getName();
    			 }
    			 
    			 if(cookieName.equals("login"))
    			 {
    				 response.sendRedirect("BalancePage");
    				 return;
    			  }
    		 }
    		}
	
	
    if (ec2 == null) {
        AWSCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider();
        ec2    = new AmazonEC2Client(credentialsProvider);
        s3     = new AmazonS3Client(credentialsProvider);
        dynamo = new AmazonDynamoDBClient(credentialsProvider);
    }
%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<title>Hello AWS Web World!</title>
<link rel="stylesheet" href="styles/styles.css" type="text/css"
	media="screen">

<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.4.3.min.js"></script>
<script type="text/javascript" src="js/loginTools.js"></script>

</head>

<body>
<h1>
Bank Portal Login
</h1>

	<form action="login_form">
		<!-- put page to send data to here -->
		Login: <input type="text" name="Login" value="" id="loginInput"><br>
		Password: <input type="text" name="Password" value=""
			id="passwordInput"><br>
		<button type="button" onclick="window.loginTools.login();">Login</button>
		<a href="url">Forgot your password?</a> <br> <a
			href="Registration">Register</a>
	</form>

</body>
</html>