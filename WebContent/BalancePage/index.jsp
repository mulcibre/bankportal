<html ng-app="balanceSheetApp">
    <head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>
    Online Banking Balance Sheet Dev
</title>
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.3.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.js"></script>
  <script type="text/javascript" src="rows.js"></script>
  
  <link rel="stylesheet" type="text/css" href="styles/normalize.css">
  
  <!-- this css doesn't appear to be used, so we'll leave it out to eliminate site errors
  <link rel="stylesheet" type="text/css" href="/css/result-light.css">
  -->
  <style type="text/css">
    * { font-family:Arial; }
    h1 {height: 35px; padding-top: 5px;}
h2 { padding:0 0 5px 5px; }
h2 a { color: #224f99; }
a { color:#999; text-decoration: none; }
a:hover { color:#802727; }
p { padding:0 0 5px 0; }

input { padding:5px; border:1px solid #999; border-radius:4px; -moz-border-radius:4px; -web-kit-border-radius:4px; -khtml-border-radius:4px; }
  </style>
  


</head>
<h1 style="display:inline-block; margin: 0px 0px 0px 0px;"> Online Banking Balance Sheet Dev</h1>
<img hidden=true style="display:inline-block" id="savingImage" src="Saving.gif"/>

<style> 
#main {
    width: 550px;
    height: 15px;
    display: -webkit-flex; /* Safari */
    -webkit-align-items: center; /* Safari 7.0+ */
    display: flex;
    text-align: center;
    padding-bottom: 5px;
}

#main div {
   -webkit-flex: 1; /* Safari 6.1+ */
   flex: 1;
}
</style>

    <body>
        <div id="addRow">
            <p>
                <label for="activityToAdd"><input type="text" id="activityToAdd" size="20" value="" placeholder="Activity"></label>
                <label for="expenseToAdd"><input type="text" onkeypress='window.rowUtils.forceValueType(event)' id="expenseToAdd" size="20" value="" placeholder="Expense"></label>
                <label for="incomeToAdd"><input type="text" onkeypress='window.rowUtils.forceValueType(event)' id="incomeToAdd" size="20" value="" placeholder="Income"></label>
            <button onClick="window.rowUtils.createRow()">Add Another Row</button>
            </p>
        </div>
        
        
        <br><br>
        
        <div id="main">
           <div style="background-color:coral;width:146px;padding:8px 8px 8px 8px;">Activity</div>
           <div style="background-color:lightblue;width:146px;padding:8px 8px 8px 8px;">Expense</div>  
           <div style="background-color:coral; width:146px;padding:8px 8px 8px 8px;;">Income</div>
        </div>
        
        <div ng-controller = "rowController">
        	<div ng-repeat = "row in rows">
        		 <p>
                	<label for="activityToAdd"><input ng-change="rowChanged(row.transactionId)" ng-model="row.description" type="text" id="activityToAdd" size="20" value="" placeholder="Activity"></label>
                	<label for="expenseToAdd"><input ng-change="rowChanged(row.transactionId)" ng-model="row.expense" type="text" onkeypress='window.rowUtils.forceValueType(event)' id="expenseToAdd" size="20" value="" placeholder="Expense"></label>
                	<label for="incomeToAdd"><input ng-change="rowChanged(row.transactionId)" ng-model="row.income" type="text" onkeypress='window.rowUtils.forceValueType(event)' id="incomeToAdd" size="20" value="" placeholder="Income"></label>
                	<button ng-click="deleteRow(row.transactionId)"> Remove </button>
           		</p>
        	</div>    
        </div>
        
        <br><br>
        
        
        <div id="subTotalsWindow">
            <p> 
                <span style="padding-left:159px"> Totals:
                <label for="expenseTotal"><input type="text" id="expenseTotal" size="20" name="expenseTotal" value="" placeholder="Expense Total"></label>
                <label for="incomeTotal"><input type="text" id="incomeTotal" size="20" name="incomeTotal" value="" placeholder="Income Total"></label>
            </p>
        </div>
        
        <div id="balanceWindow">
            <p> 
                <span style="padding-left:144px"> Balance:
                <label for="balance"><input type="text" id="balance" size="20" name="balance" value="" placeholder="Balance"></label>
                <button style="margin-left: 25px" onClick="window.logout()">Logout</button>
                </p>
                
        </div>
        
        
   
        
    </body>
</html>