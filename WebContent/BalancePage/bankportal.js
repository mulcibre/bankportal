function testSave() {
	var transaction1 = {
		description : "face",
		income : 10.55,
		expense : 0
	};
	var transaction2 = {
		description : "next",
		income : 0,
		expense : 8.75
	};
	var transactions = [ transaction1, transaction2 ];

	$.ajax({
		type : 'POST',
		url : '../transaction',
		data : JSON.stringify(transactions),
		success : function(data) {
			alert('data: ' + data);
		},
		contentType : "application/json",
		dataType : 'json'
	});

}

function testLoad() {

	$.ajax({
		type : 'GET',
		url : '../transaction',
		success : function(data) {
			alert('data: ' + data);
		},
		contentType : "application/json",
		dataType : 'json'
	});

}

$(window)
		.load(
				function() {

					$(function createBoxs() {
						var scntDiv = $('#p_scents');
						var i = $('#p_scents p').size() + 1;

						$('#addScnt')
								.live(
										'click',
										function() {

											$(
													'<p>	<label for="activity"><input type="text" id="activity" size="20" name="activity_'
															+ i
															+ '" value="" placeholder="Activity" /></label>'
															+ '<label for="expense"><input type="text" id="expense" size="20" name="expense_'
															+ i
															+ '" value="" placeholder="Expense" /></label>'
															+ '<label for="income"><input type="text" id="income" size="20" name="income_'
															+ i
															+ '" value="" placeholder="Income" /></label>'
															+ '<a href="#" id="removeRow">Remove</a>'
															+ '</p>').appendTo(
													scntDiv);

											i++;
											return false;
										});

						$('#remScnt').live('click', function() {
							if (i > 2) {
								$(this).parents('p').remove();
								i--;
							}
							return false;
						});
					});

				});// ]]>
