(function()
{
	var rows = window.rowUtils = window.rowUtils || {};

	// Flag which will be set if a network call to the server is underway
	var _inFlight = false;
	$("#savingImage").hide();
	// Flag which will be set if the row data needs to be synced
	var _shouldSync = false;

	$(document).ready(function onLoadGetRows()
	{
		window.rowsToSync = {};

		$.ajax(
		{
			type : 'GET',
			url : '../transaction',

			success : function(data)
			{
				if (data)
				{
					window.updateRows(data);
					rows.calculateTotal();
				}
				else
				{

				}
			},

			contentType : "application/json",
			dataType : 'json'
		});
		setupAngular();

		$("#savingImage").hide();
	});

	rows.createRow = function createRow()
	{
		var activityToAdd = $("#activityToAdd").val();
		var expenseToAdd = $("#expenseToAdd").val();
		var incomeToAdd = $("#incomeToAdd").val();

		$("#activityToAdd").val("");
		$("#expenseToAdd").val("");
		$("#incomeToAdd").val("");

		var timeStamp = new Date().getTime();

		var rowToCreate =
		{
			transactionId : "new",
			description : activityToAdd,
			expense : expenseToAdd,
			income : incomeToAdd,
			timeStamp : timeStamp
		};

		var transactionData = prepareRowsForNetwork([ rowToCreate ]);
		if (transactionData.length < 1)
		{
			alert("Please make sure that expense and income fields are in correct format");
			return;
		}

		$.ajax(
		{
			type : 'POST',
			url : '../transaction',
			data : JSON.stringify(transactionData),

			success : rowAddedSuccess,

			contentType : "application/json",
			dataType : 'json'
		});
	};

	rows.deleteRow = function deleteRow(transactionId)
	{

		$.ajax(
		{
			type : 'DELETE',
			url : '../transaction?transactionId=' + transactionId,

			success : rowDeletedSuccess,

			contentType : "application/json",
			dataType : 'json'
		});
	};

	window.logout = function window$logout()
	{
		document.cookie = 'login=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/;';
		setTimeout(function (){
			window.location = qualifyURL("../");
		},0);
	};

	rows.verifyIncome = rows.verifyExpense = function rows$verifyValue(value)
	{
		if (value === '.' || value === "")
		{
			value = 0;
		}

		var num = parseFloat(value);

		if (isNaN(num))
		{
			return false;
		}
		if (num < 0)
		{
			num = num * -1;
		}
		return Math.round(num * 100) / 100;
	};

	var valueTypeRegex = /^[0-9]*\.?[0-9]*$/;

	rows.forceValueType = function forceValueType(evt)
	{
		var theEvent = evt || window.event;
		var key = theEvent.keyCode || theEvent.which;
		key = theEvent.srcElement.value + String.fromCharCode(key);
		if (!valueTypeRegex.test(key))
		{
			theEvent.returnValue = false;
			if (theEvent.preventDefault)
				theEvent.preventDefault();
		}
	};

	function setupAngular()
	{
		var balanceSheetApp = window.balanceSheetApp = angular.module(
				"balanceSheetApp", []);

		balanceSheetApp
				.controller(
						'rowController',
						function($scope)
						{
							$scope.rows = window.rows = window.rows || [];

							$scope.rowChanged = function(transactionId)
							{
								_shouldSync = false;
								clearTimeout(window.syncTimer);
								window.syncTimer = setTimeout(syncRowsToServer,
										2000);
								window.rowsToSync[transactionId] = window.rowLookup[transactionId];

								// update totals
								rows.calculateTotal();
							};

							$scope.deleteRow = function(transactionId)
							{
								window.rowUtils.deleteRow(transactionId);
							};
						});

		angular
				.element(document)
				.ready(
						function()
						{
							var appElement = document
									.querySelector('[ng-app=balanceSheetApp]');
							var appScope = angular.element(appElement).scope();

							var controllerElement = document
									.querySelector('body');
							var controllerScope = angular.element(
									controllerElement).scope();

							window.updateRows = function(newRows)
							{
								controllerScope
										.$apply(function()
										{
											// /
											// This method combines the "rows"
											// from the old array located at
											// $scope.rows and window.rows,
											// with the new array of rows
											// received from the server into the
											// original array, sorted by
											// timestamp.
											// 1) Place elements from
											// window.rows into timeStampToRows
											// object. using their timestamp as
											// keys
											// 2) Place elements from newRows
											// into timeStampToRows object,
											// overriding previous rows on the
											// client
											// 3) Sort the keys (timestamps)
											// from the union of all rows
											// 4) Place sorted union of rows
											// into original rows array
											// /

											var rows = window.rows = window.rows
													|| [];
											var rowLookup = window.rowLookup = window.rowLookup
													|| {};

											var timeStampToRows = {};

											// 1) Place elements from
											// window.rows into timeStampToRows
											// object. using their timestamp as
											// keys
											for (var i = 0; i < rows.length; i++)
											{
												timeStampToRows[rows[i].timeStamp] = rows[i];
											}

											// 2) Place elements from newRows
											// into timeStampToRows object,
											// overriding previous rows on the
											// client
											for (var i = 0; i < newRows.length; i++)
											{
												timeStampToRows[newRows[i].timeStamp] = newRows[i];
											}

											// 3) Sort the keys (timestamps)
											// from the union of all rows
											var keys = [];
											for ( var key in timeStampToRows)
												keys.push(key);
											// sort keys
											keys.sort(sortNumber);

											// 4) Place sorted union of rows
											// into original rows array
											for (var i = 0; i < keys.length; i++)
											{
												rows[i] = timeStampToRows[keys[i]];
												rowLookup[rows[i].transactionId] = rows[i];
											}

										});
							};

							window.deleteRow = function(transactionId)
							{
								controllerScope
										.$apply(function()
										{

											var rows = window.rows = window.rows
													|| [];
											var rowLookup = window.rowLookup = window.rowLookup
													|| {};

											for (var i = rows.length - 1; i >= 0; i--)
											{
												if (window.rows[i].transactionId === transactionId)
												{
													window.rows.splice(i, 1);
													break;
												}
											}

											delete rowLookup[transactionId];

										});
							};

						});
	}

	function syncRowsToServer()
	{
		// if a network call is already being made to the server, don't send
		// another
		_shouldSync = true;
		if (_inFlight)
		{
			return;
		}

		var rowsToSync = window.rowsToSync;
		var preparedRows = prepareRowsForNetwork(rowsToSync);
		var rowsInFlight = window.rowsInFlight = [];

		for (var i = 0; i < preparedRows.length; i++)
		{
			var preparedRow = preparedRows[i];
			var row = rowsToSync[preparedRow.transactionId];
			if (row)
			{
				rowsInFlight.push(row);
			}

			delete rowsToSync[preparedRow.transactionId];
		}

		_inFlight = true;
		$("#savingImage").show();
		_shouldSync = false;
		$.ajax(
		{
			type : 'POST',
			url : '../transaction',
			data : JSON.stringify(preparedRows),

			success : rowsUpdatedSuccess,

			contentType : "application/json",
			dataType : 'json'
		});
	}

	function sortNumber(a, b)
	{
		return a - b;
	}

	function prepareRowsForNetwork(rows)
	{
		// return an array of transactionDatas which will be grabbed from
		// the rows passed in
		var retVal = [];
		rows = objectToArray(rows);

		for (var i = 0; i < rows.length; i++)
		{
			var expenseToCheck = rows[i].expense;
			var incomeToCheck = rows[i].income;
			var description = rows[i].description;
			expenseToCheck = window.rowUtils.verifyExpense(expenseToCheck);
			incomeToCheck = window.rowUtils.verifyIncome(incomeToCheck);

			if (description.length < 1 || typeof (expenseToCheck) !== "number"
					|| typeof (incomeToCheck) !== "number")
			{
				continue;
			}

			var transactionData =
			{
				transactionId : rows[i].transactionId,
				description : rows[i].description,
				expense : expenseToCheck,
				income : incomeToCheck,
				timeStamp : rows[i].timeStamp
			};
			retVal.push(transactionData);
		}
		return retVal;
	}

	function objectToArray(obj)
	{
		if ($.isArray(obj))
		{
			return obj;
		}
		if ($.isPlainObject(obj))
		{
			var retVal = [];

			for ( var key in obj)
			{
				if (obj.hasOwnProperty(key))
				{
					retVal.push(obj[key]);
				}
			}
			return retVal;
		}
		throw "object was not appropriate type for conversion";
	}

	function rowAddedSuccess(data)
	{
		window.updateRows(data);
		rows.calculateTotal();
	}

	function rowDeletedSuccess(data)
	{
		window.deleteRow(data);
		rows.calculateTotal();
	}

	rows.calculateTotal = function calculateTotal()
	{
		var expenseTotal = 0;
		var incomeTotal = 0;
		var rows = window.rows = window.rows || [];

		for (var i = 0; i < rows.length; i++)
		{
			var temp = window.rowUtils.verifyIncome(rows[i].income);
			if (temp)
			{
				incomeTotal += temp;
			}

			temp = window.rowUtils.verifyExpense(rows[i].expense);
			if (temp)
			{
				expenseTotal += temp;
			}
		}
		$("#expenseTotal").val(window.rowUtils.verifyExpense(expenseTotal));
		$("#incomeTotal").val(window.rowUtils.verifyExpense(incomeTotal));
		$("#balance").val(
				window.rowUtils.verifyExpense(incomeTotal - expenseTotal));
	};

	function rowsUpdatedSuccess(data)
	{
		window.updateRows(data);
		_inFlight = false;
		$("#savingImage").hide();
		if (_shouldSync)
		{
			syncRowsToServer();
		}
	}

	function escapeHTML(s)
	{
		return s.split('&').join('&amp;').split('<').join('&lt;').split('"')
				.join('&quot;');
	}

	function qualifyURL(url)
	{
		var el = document.createElement('div');
		el.innerHTML = '<a href="' + escapeHTML(url) + '">x</a>';
		return el.firstChild.href;
	}

}());