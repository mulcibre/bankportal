(function(){
	
	var lt = window.loginTools = window.loginTools || {};
	
	lt.login = function login()
    {
    	var username = $("#loginInput").val();
    	var password = $("#passwordInput").val();
    	
    	//create a check for null values
    	if(!username || username == "" || 
    			!password ||  password == "")
    		{
    		alert("Please fill in login fields");
    		return;
    		}
    	//send login data for user validation
		var loginData = {username:username, password:password}
		
		$.ajax({
	        type: 'POST',
	        url: 'login',
	        data: JSON.stringify(loginData),
	        
	        success: function (data) 
	        {
	        	if(data)
	        		{
	        		window.location = qualifyURL("BalancePage");
	        		}
	        	else
	        		{
	        		alert("Username/password incorrect!");
	        		}
	        	},
	        	
	        contentType: "application/json",
	        dataType: 'json'
	    });
    }
	
	function escapeHTML(s) {
	    return s.split('&').join('&amp;').split('<').join('&lt;').split('"').join('&quot;');
	}
	
	function qualifyURL(url) {
	    var el= document.createElement('div');
	    el.innerHTML= '<a href="'+escapeHTML(url)+'">x</a>';
	    return el.firstChild.href;
	}
	
}());